# Week 6

# Agenda

- The design of the sprayer system was sketced. 
- Payload mounting placement on gondola was discussed and the calculation of parameters were done.
- Ensure the component selected is available on market by contact the supplier/seller before proceed with purchase.


# Impact

- Electrical PVC pipe was used as the mounting body for the sprayer systems because it is lighter than regular PVC pipe.
- The pipe will be joined together with connectors to form the structure.
- Once the component is finalized, the weight of the structure can be estimate.

