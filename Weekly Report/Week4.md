# Week 4

# Agenda

- Calculation parameter for sprayer.
- Study the components and structure of a sprayer.

# Impact

_Meeting 1_

- Sprayer calculation and calibration sheet created by Thebban and Yamunan
- Placement of the sprayer below the gondola 
- Swath overlap of the sprayer between 50% to 100%
- Choosing Y-shape or L/T shape for nozzle

_Meeting 2_
- Suggested pump with 8L/min might be too fast for HAU
- Need to find out if normal water pump could be used instead of agri drone pumps
- 5L tank is sufficient for small airship
- Field speed is set to be slow as HAU moves slow and improves battery life
