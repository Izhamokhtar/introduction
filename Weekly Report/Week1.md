# Week 1

For the first week of semester, we were introduced to Integrated Design Project (IDP) course that were instructed by Dr Salahuddin with the help of Mr Amirul Fiqri. We were briefed about our project which was named Putraspace Hybrid Airship UAV (PHSAU). We were divided into several subsystems to help students achieve the project objectives smoothly. There are 6 category of subsystems which are Flight system integration, Design, Simulation, Propulsion, Control Systems and Payload (Agricultural Sprayer). To ensure the success of this project, our instructor has provide us with a flowchart that will be our guideline througout this semester.

<img src="Figures/IDP_Development_Flowchart-Airship.drawio.png" alt="screenshot" width="1000"/>
