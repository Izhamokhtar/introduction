# Week 7

# Agenda

- Purchase the finalized component on nearest store.
- For online buying, the component has been ordered and expected to receive on next week.


# Impact

- Component that we bought from the store has been connected to structure. 

