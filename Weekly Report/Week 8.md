# Week 8

# Agenda

- Boomstick was assembled by using pvc and the fluxtube.
- Design of the placement of the payload on the gondola was discussed.

# Impact

- The design of the payload placement need to be redesign
- The design need to meet with a certain requirement that such as battery access, power board access, and thruster arm connector.
