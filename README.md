## INTRODUCTION


### Biography

<img src="https://gitlab.com/Izhamokhtar/introduction/-/raw/main/Izham/MuhammadIzham_Passport.jpg" width=150 align=middle>

* *NAME:* MUHAMMAD IZHAM BIN MOHD MOKHTAR

* *AGE:* 22

* *MATRIC NO:* 196417

* *COURSE:* Bachelor Of Aerospace Engineering  With Honors

## Strength & Weakness

| Strength   | Weakness   |
| :--------:   | :--------: |
| strategist   | hard to say no |
| team player  | impulsive | 
| good listener| lazy      |
| talkative    | undecisive|

